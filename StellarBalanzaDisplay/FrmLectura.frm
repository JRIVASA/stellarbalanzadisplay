VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSComm32.ocx"
Begin VB.Form FrmLectura 
   ClientHeight    =   960
   ClientLeft      =   120
   ClientTop       =   450
   ClientWidth     =   1740
   LinkTopic       =   "Form1"
   ScaleHeight     =   960
   ScaleWidth      =   1740
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin MSCommLib.MSComm Balanza 
      Left            =   900
      Top             =   180
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.Timer LectorContinuo 
      Enabled         =   0   'False
      Left            =   300
      Top             =   240
   End
End
Attribute VB_Name = "FrmLectura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Form_Load()
    On Error GoTo Error
    If Not Inicializado Then
        Inicializado = True
        If Not Configuracion("Simulacion") Then
            Balanza.CommPort = Configuracion("Puerto")
            Balanza.Settings = Configuracion("Datos")
            'On Error Resume Next
            'MsgBox Balanza.CommPort & Balanza.Settings & vbNewLine _
            & Configuracion("Puerto") & Configuracion("Datos") & vbNewLine & _
            Configuracion("Frecuencia") & vbNewLine & _
            Configuracion("BalanzaControlRTSHand")
            Balanza.PortOpen = True
            'MsgBox "Inicializando Puerto: " & Balanza.PortOpen
            If Configuracion("BalanzaControlRTSHand") Then
                Balanza.RThreshold = 1
                Balanza.Handshaking = comNone
                Balanza.RTSEnable = True
            End If
        End If
        LectorContinuo.Enabled = False
        LectorContinuo.Interval = Configuracion("Frecuencia")
        LectorContinuo.Enabled = True
        Inicializando = False
        Exit Sub
    End If
    Exit Sub
Error:
    Inicializando = False
    Inicializado = False
    End
End Sub

Public Sub LectorContinuo_Timer()
    If EnProceso Then
        ' NO HACER ESTE CICLO... CUELGA EL CALLER APP.
        'While EnProceso
            'DoEvents
        'Wend
        Exit Sub
    Else
        TmpUltimaLectura = LecturaBalanzaSerial
    End If
End Sub
