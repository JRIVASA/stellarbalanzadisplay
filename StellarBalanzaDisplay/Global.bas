Attribute VB_Name = "Global"
Public TmpForm                      As FrmLectura
Public SharedCls                    As Handler
Public EnProceso                    As Boolean
Public TmpUltimaLectura             As String
Public Configuracion                As Dictionary
Public Inicializado                 As Boolean
Public Inicializando                As Boolean

Public Sub Main()
    If App.PrevInstance Then
        End
    End If
End Sub

Public Sub IniciarLectura()
    If Not Inicializado And Not Inicializando Then
        Inicializando = True
        Set TmpForm = New FrmLectura
        Load TmpForm
    End If
End Sub

Public Sub DetenerLectura()
    If Inicializado Then
        TmpForm.LectorContinuo.Enabled = False
        If EnProceso Then
            While EnProceso
                DoEvents
            Wend
        End If
        Set TmpForm = Nothing
        Inicializado = False
    End If
End Sub

Public Function LecturaBalanzaSerial() As String
    
    On Error GoTo ErrorBalanza
    
    EnProceso = True
    
    If Configuracion("Simulacion") Then
        TmpVal = Rnd * 4
        If TmpVal < 0.01 Then TmpVal = 0
        LecturaBalanzaSerial = CStr(TmpVal)
        If Configuracion("DebugMode") Then MsgBox LecturaBalanzaSerial
        GoTo Finally
    End If
    
    Dim sTemp As Variant, sValidar As Variant, ContadorDecimal As Integer
    
    ContadorDecimal = 0
    
    'MsgBox "Caracter_Control: " & Configuracion("Caracter_Control")
    'MsgBox "BalanzaTeclaAlfinalComando: " & Configuracion("BalanzaTeclaAlfinalComando")
    'MsgBox "BalanzaManejaPuntoDecimal: " & Configuracion("BalanzaManejaPuntoDecimal")
    'MsgBox "BalanzaValidarPeso: " & Configuracion("BalanzaValidarPeso")
    'MsgBox "ScannerNumeroCiclosparaCaptura: " & Configuracion("ScannerNumeroCiclosparaCaptura")
    'MsgBox "BalanzaPosInicio: " & Configuracion("BalanzaPosInicio")
    
    'MsgBox "Puerto: " & TmpForm.Balanza.PortOpen
    
    If Not TmpForm.Balanza.PortOpen Then TmpForm.Balanza.PortOpen = True
    
    If Configuracion("DebugMode") Then MsgBox "Puerto: " & TmpForm.Balanza.PortOpen
    
    If TmpForm.Balanza.PortOpen Then
        
        Dim A As Integer, M
        A = 0
        
        TmpForm.Balanza.OutBufferCount = 0
        TmpForm.Balanza.InBufferCount = 0
        
        Dim mCiclos
        Const mCiclosAvg = 50
        
        If Configuracion("ScannerNumeroCiclosparaCaptura") < mCiclosAvg Then
            mCiclos = mCiclosAvg
        Else
            mCiclos = Configuracion("ScannerNumeroCiclosparaCaptura")
        End If
        
        For i = 1 To mCiclos
            
            TmpForm.Balanza.InputLen = 0
            M = TmpForm.Balanza.InBufferCount
            'MsgBox Configuracion("Caracter_Control") & _
            IIf(Configuracion("BalanzaTeclaAlfinalComando"), Chr$(Configuracion("BalanzaTeclaAlfinalComando")), "")
            TmpForm.Balanza.Output = Configuracion("Caracter_Control") & _
            IIf(Configuracion("BalanzaTeclaAlfinalComando"), Chr$(Configuracion("BalanzaTeclaAlfinalComando")), "")
            
            sTemp = TmpForm.Balanza.Input
            
            mLontemp = Len(sTemp)
            
            If Configuracion("DebugMode") Then MsgBox "sTemp: " & sTemp & mLontemp
            
            msTemp = ""
            
            If sTemp <> "" Then
            
                For qq = 1 To mLontemp
                    mcar = Mid(sTemp, qq, 1)
                    If IsNumeric(mcar) Then
                        msTemp = msTemp & mcar
                    ElseIf Mid(mcar, 1, 1) = "." And Configuracion("BalanzaManejaPuntoDecimal") Then
                        If InStr(1, msTemp, ".") = 0 Then msTemp = msTemp & mcar
                    End If
                Next
                
                sTemp = msTemp
                
                'Montes
                
                If Configuracion("BalanzaValidarPeso") And Configuracion("BalanzaManejaPuntoDecimal") Then
                    If IsNumeric(sTemp) Then
                        'Debug.Print "STEMP=" & sTemp
                        'Debug.Print "SVALIDAR=" & sValidar
                        If sValidar = "" Then sValidar = sTemp
                    End If
                End If
                
                If Configuracion("BalanzaPosInicio") <> 0 Then
                    'If DebugMode And A < 10 Then Mensaje True, sTemp & GetLines(2) & Mid(sTemp, Configuracion("BalanzaPosInicio"))
                    sTemp = Mid(sTemp, Configuracion("BalanzaPosInicio"))
                End If
                
            End If
            
            ' NO HACER DOEVENTS NI EL CICLO.... DA�AN LA LECTURA CONTINUA.
            
            'DoEvents
            
            'For X = 1 To 50
                
            'Next X
            
            'If Configuracion("DebugMode") Then MsgBox "sTemp: " & sTemp & mLontemp
            
            If sTemp <> "" Then
                A = A + 1
                If Configuracion("ScannerNumeroCiclosparaCaptura") = A Then
                    Exit For
                End If
            End If
            
        Next
        
    End If
    
    'If Configuracion("DebugMode") Then MsgBox "sTemp: " & sTemp
    
    If Len(sTemp) = 0 Then
        GoTo Finally
    End If
    
    'If DebugMode Then Mensaje True, CStr(sTemp) & GetLines(2) & "BalanzaPosInicio: " & Configuracion("BalanzaPosInicio")
    
    mPosicion = IIf(Configuracion("BalanzaPosInicio") <> 0, Configuracion("BalanzaPosInicio"), 2)
    
    If Not IsNumeric(Mid(sTemp, 2, Len(sTemp) - 1)) Then
        GoTo Finally
    End If
    
    If sTemp <> "" Then
        
        Dim MontoBal As Double
        
        MontoBal = Mid(sTemp, CLng(mPosicion), Len(sTemp))
        
        If IsNumeric(MontoBal) Then
            If Not Configuracion("BalanzaManejaPuntoDecimal") Then  'glbalanza_manejapuntodecimal
                MontoBal = MontoBal / 1000
                sTemp = MontoBal
            Else
                sTemp = FormatNumber(MontoBal, 3)
            End If
            
            'MontoBal = FormatNumber(MontoBal, 3)
        End If
        
    End If
    
    If Configuracion("BalanzaValidarPeso") And Configuracion("BalanzaManejaPuntoDecimal") Then
        If sValidar <> "" And IsNumeric(sTemp) Then
            If sValidar <> sTemp Then sTemp = ""
        End If
    End If
    
    LecturaBalanzaSerial = sTemp
    
Finally:
    
    If Configuracion("DebugMode") Then MsgBox "LecturaBalanzaSerial: " & LecturaBalanzaSerial
    
    On Error Resume Next
    
    'If TmpForm.Balanza.PortOpen Then TmpForm.Balanza.PortOpen = False
    
    EnProceso = False
    
    Exit Function
    
ErrorBalanza:
    
    mErrorNumber = Err.Number
    mErrorDesc = Err.Description
    mErrorSource = Err.Source
    
    If Configuracion("DebugMode") Then MsgBox mErrorDesc & " " & "(" & mErrorNumber & ")."
    
    Resume Finally
    
End Function
