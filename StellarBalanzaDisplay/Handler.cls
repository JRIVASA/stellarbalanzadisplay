VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 1  'Persistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Handler"
Attribute VB_GlobalNameSpace = True
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Property Get UltimaLectura() As String
    UltimaLectura = TmpUltimaLectura
End Property

Property Let Parametros(ByVal pValor As Dictionary)
    Set Configuracion = pValor
End Property

Public Sub Inicializar()
    IniciarLectura
End Sub

Public Sub Detener()
    DetenerLectura
End Sub

Private Sub Class_Initialize()
    Set SharedCls = Me
End Sub

Public Function DevolverLectura() As String
    If Inicializado And Not Inicializando Then
        TmpForm.LectorContinuo_Timer
    End If
    DevolverLectura = UltimaLectura
End Function
